#! /usr/bin/env python
# -*- coding: utf-8 -*-

import random
import string

import pandas as pd
import pytest


@pytest.fixture()
def setup(storageTest):
    # Setup directories for unit testing

    dirname = "unittest"
    filename = "test.csv"
    data = pd.DataFrame({"a": [1, 2, 3], "b": [4, 5, 6]})
    storageTest.create_directory(directory=dirname)
    storageTest.push_data(
        directory=dirname, remote_filename=filename, data=data, overwrite=True
    )

    yield dirname, filename, data


def _random_string(L=10):
    return "".join(random.choices(string.ascii_lowercase, k=L))


def _random_tag_dict(N=3, L=4):
    result = dict()
    for _ in range(N):
        result[_random_string(L)] = _random_string(L)
    return result


def _random_tag_dict_including_common_tag(N=3, L=4):
    result = _random_tag_dict(N - 1, L)
    result["common_tag"] = _random_string(L)
    return result


def _random_pair_of_common_tag_dict(N=3, L=4):
    return (
        _random_tag_dict_including_common_tag(N, L),
        _random_tag_dict_including_common_tag(N, L),
    )


@pytest.mark.parametrize(
    "tag_dict_pair", [_random_pair_of_common_tag_dict() for _ in range(3)]
)
def test_set_get_tags(setup, storageTest, tag_dict_pair):
    """Set tags (with overwrite), then get them."""

    dirname, filename, _ = setup
    tags1, tags2 = tag_dict_pair

    # set to tags1 and check with a get
    storageTest.set_tags(dirname, filename, False, **tags1)
    assert storageTest.get_tags(dirname, filename) == tags1

    # set to tags1 and check with a get
    storageTest.set_tags(dirname, filename, False, **tags2)
    assert storageTest.get_tags(dirname, filename) == tags2


@pytest.mark.parametrize(
    "tag_dict_pair", [_random_pair_of_common_tag_dict() for _ in range(3)]
)
def test_update_tags(setup, storageTest, tag_dict_pair):
    """Update tags (without overwrite)."""

    dirname, filename, _ = setup
    tags1, tags2 = tag_dict_pair

    # set to tags1, then update to tags2
    storageTest.set_tags(dirname, filename, False, **tags1)
    storageTest.set_tags(dirname, filename, True, **tags2)

    # check result
    expected_tags = dict()
    expected_tags.update(tags1)
    expected_tags.update(tags2)
    assert storageTest.get_tags(dirname, filename) == expected_tags
