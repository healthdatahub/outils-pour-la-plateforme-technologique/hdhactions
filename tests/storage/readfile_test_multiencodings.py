#! /usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import json
import os

import pandas as pd
import pytest


@pytest.fixture()
def setup(storageTest):
    # Setup directories for unit testing
    yield storageTest.create_directory(directory="unittest")


# Test assets
_ENCO_LIST = [
    "utf-8",
    "latin-1",
]

_STR_LIST = [
    "Hello world",
    "Ô",
    "∂T/∂t=α ∂²T/∂x²",
    "石室詩士施氏，嗜獅，誓食十獅。",
]

_JSON_STR_LIST = []
_JSON_STR_LIST.append(
    r"""{
    "greek": {
        "alpha": "α",
        "beta": "β"
        },
    "O circumflex": "Ô"
}"""
)


def _generate_test_pairs(str_list, enco_list):
    """
    Check what combinations of (string, encoding) are possible.

    Return the list of all compatible 2-uples.
    """
    result = []
    for strstri, enco in itertools.product(str_list, enco_list):
        try:
            strstri.encode(encoding=enco)
            result.append((strstri, enco))
        except UnicodeEncodeError:
            pass  # do not add this combination to the test list

    return result


def test_readfile_csv(setup, storageTest):
    filename = "tempfile.csv"
    directory = "unittest"
    try:
        # Try all combinations of test string / encoding
        for test_str, encoding in _generate_test_pairs(_STR_LIST, _ENCO_LIST):
            # Create the file with the encoding
            test_df = pd.DataFrame({"text": [test_str, test_str]})
            test_df.to_csv(filename, index=False, encoding=encoding)

            # Push the file as-is
            storageTest.push_file(
                directory=directory, local_filename=filename, overwrite=True
            )

            # Read it and see if it has the expected contents
            df_around = storageTest.read_file(
                directory=directory,
                filename=filename,
                fmt="csv",
                zip=False,
                encoding=encoding,
            )

            assert test_df.equals(df_around)
    finally:
        os.remove(filename)
        storageTest.delete_file(directory=directory, filename=filename)


def test_readfile_txt(setup, storageTest):
    filename = "tempfile.json"
    directory = "unittest"
    encoding = "utf8"
    try:
        for test_str, encoding in _generate_test_pairs(_STR_LIST, _ENCO_LIST):
            # Push the file with that encoding
            with open(filename, "w", encoding=encoding) as fifi:
                fifi.write(test_str)

            storageTest.push_file(
                directory=directory, local_filename=filename, overwrite=True
            )

            # Read it as text and check contents
            txt_read = storageTest.read_as_txt(
                directory=directory,
                filename=filename,
                zip=False,
                encoding=encoding,
            )

            assert txt_read == test_str

    finally:
        os.remove(filename)
        storageTest.delete_file(directory=directory, filename=filename)


def test_readfile_json(setup, storageTest):
    filename = "tempfile.json"
    directory = "unittest"

    try:

        for test_str, encoding in _generate_test_pairs(_JSON_STR_LIST, _ENCO_LIST):
            # Push the file with that encoding
            with open(filename, "w", encoding=encoding) as fifi:
                fifi.write(test_str)

            storageTest.push_file(
                directory=directory, local_filename=filename, overwrite=True
            )

            # Read it as JSON and check contents
            json_read = storageTest.read_file_json_into_dict(
                directory=directory,
                filename=filename,
                zip=False,
                encoding=encoding,
            )

            assert json_read == json.loads(test_str)

    finally:
        os.remove(filename)
        storageTest.delete_file(directory=directory, filename=filename)
