# -*- coding: utf-8 -*-
"""
     conftest.py for hdhactions.

    If you don't know what this is for, just leave it empty.
    Read more about conftest.py under:
    - https://docs.pytest.org/en/stable/fixture.html
    - https://docs.pytest.org/en/stable/writing_plugins.html
"""

import os

import pytest

from .conf import AzureStorageTest


def pytest_configure():
    pytest.azurite_host = "http://%s:10000" % (os.environ["HOST"])
    pytest.storage_key = "Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw=="  # noqa
    pytest.account_name = "devstoreaccount1"
    pytest.connection_string = f"DefaultEndpointsProtocol=http;AccountName={pytest.account_name};AccountKey={pytest.storage_key};BlobEndpoint={pytest.azurite_host}/{pytest.account_name};"  # noqa


@pytest.fixture()
def storageTest():
    return AzureStorageTest()
