#! /usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import os
import pickle as pkl
import string
import warnings
import zipfile
from collections import defaultdict
from io import BytesIO
from itertools import islice
from typing import Any, BinaryIO, Dict, Iterable, List, Optional, Union

import h5py
import pandas as pd
from azure.core.exceptions import ClientAuthenticationError
from azure.storage.blob import BlobClient, ContainerClient, StorageStreamDownloader

from .config import Config


class HdhactionsError(Exception):
    pass


class HdhactionsAlreadyExistsError(HdhactionsError):
    """When you want to create something that already exists"""

    pass


class HdhactionsMissingError(HdhactionsError):
    """When you want to access something that does not exist"""

    pass


class HdhactionsInvalidContainerName(HdhactionsError):
    """When a container name does not conform to the standard."""

    pass


# Storage names that can be interpeted from os variables
_special_storage_names = {
    "storage_workspace": "storage_workspace",
    "workspace": "storage_workspace",
    "storage_input": "storage_input",
    "input": "storage_input",
    "storage_output": "storage_output",
    "output": "storage_output",
    "storage_external": "storage_external",
    "external": "storage_external",
}


def _iterable_chunker(iterable: Iterable, Nchunk: int):
    """Take an iterable and package it into list chunks.

    Implementation inspired from
    https://docs.python.org/3.12/library/itertools.html#itertools.batched.
    Uses the walrus operator (:=) defined in Python 3.8.

    For instance, if the iterable yields i1, i2, ... and we want chunks of 3,
    this yields [i1, i2, i3], [i4, i5, i6], ...

    Example:
        >>> for batch in _iterable_chunker(range(10), 3): print(batch)
        [0, 1, 2]
        [3, 4, 5]
        [6, 7, 8]
        [9]

    If iterable is empty, the packaged iterable is empty as well.
    Otherwise, it will return some number (possibly 0) chunks of Nchunk
    elements, followed by a last chunk of between 1 and Nchunk elements.
    """

    assert isinstance(Nchunk, int)
    assert Nchunk >= 1

    it = iter(iterable)
    while batch := list(islice(it, Nchunk)):
        yield batch


def _head_fmt(lili: list, Nmax: int = 10):

    N_present = len(lili)
    if N_present <= Nmax:
        return str(lili)
    else:
        return str(lili[:Nmax] + ["..."])


class AzureStorage(Config):
    """Interface for using the Azure storage like a normal file browser.

    Attributes:
        account_name: short name of the storage account.
          "workspace", "input", "output" and "external" are special values, which
          take the information from the appropriate environment variable.

    :Example:
    >>> from hdhactions.storage import AzureStorage
    >>> storage = AzureStorage(account_name = "trial")
    >>> # trial is the name of the storage account to work with
    """

    def __init__(self, account_name):
        if account_name in _special_storage_names:
            osvarname = _special_storage_names[account_name]
            if osvarname not in os.environ:
                raise ValueError(
                    f"The storage name '{osvarname}' refers to an environment variable, but that variable is not defined."  # noqa: E501
                )
            else:
                account_name = os.environ[osvarname]
                if len(account_name) == 0:
                    raise ValueError(
                        f"The storage name '{osvarname}' refers to an environment variable, but that variable is empty."  # noqa: E501
                    )

        # The superclass defines self.fs, self.account_name, self.blob_service_client
        super().__init__(account_name)

        # The instance is created, we check if that account really exists
        self._check_account_exists()

    def _check_account_exists(self):
        # Heuristic: call list directories() and see what happens.
        # If it does exist, execution of list_directories() will be immediate
        # If not, it will fail after a short period of time

        try:
            self.list_directories()
            logging.info(f"Account storage exists: '{self.account_name}'")
        except ClientAuthenticationError:
            raise HdhactionsMissingError(
                f"The Azure storage '{self.account_name}' does not exist"
            )

    def _check_container_name(self, cname: str):
        """Check cname is a valid container name. Error if not.

        See spec at
        https://learn.microsoft.com/en-us/rest/api/storageservices/naming-and-referencing-containers--blobs--and-metadata#container-names

        Args:
            cnam (str): the string to validate

        Raises:
            HdhactionsInvalidContainerName: if the container name is invalid.
        """

        allowed_characters = string.ascii_lowercase + string.digits + "-"

        error_prefix = f"""'{cname}' is not a valid container name.
See https://learn.microsoft.com/en-us/rest/api/storageservices/naming-and-referencing-containers--blobs--and-metadata#container-names
"""  # noqa: E501

        # Check all characters are allowed
        if set(cname) - set(allowed_characters):
            specific_error = "Container names must contain only lowercase letters, numbers, and the hyphen '-' character."  # noqa: E501
            raise HdhactionsInvalidContainerName(error_prefix + specific_error)

        # Check length
        if not (3 <= len(cname) <= 63):
            specific_error = "Container names must be between 3 and 63 characters long."
            raise HdhactionsInvalidContainerName(error_prefix + specific_error)

        # Check hyphens are well-placed
        for chunk in cname.split("-"):
            if chunk == "":
                specific_error = "Hyphens are only allowed between two non-hyphens."
                raise HdhactionsInvalidContainerName(error_prefix + specific_error)

        # If all is well, congratulations!
        logging.debug(f"Valid container name: '{cname}'")

    def _safe_get_container(
        self, cname: str, container_should_exist: bool
    ) -> ContainerClient:
        """Obtain a container client and validate whether it exists.

        Args:
            cname: name of the container
            container_should_exist: whether the container should already exist.
                If the actual status differs, raise the appropriate error.

        Returns: the ContainerClient

        Raises:
            HdhactionsInvalidContainerName: if the container name is invalid
            HdhactionsMissingError: if the container should exist but does not
            HdhactionsAlreadyExistsError: if it should not exist but it does
        """

        self._check_container_name(cname)

        container_client = self.blob_service_client.get_container_client(cname)
        if container_client.exists() != container_should_exist:
            if container_should_exist:  # should exist but does not
                raise HdhactionsMissingError(f"Directory {cname} does not exist.")
            else:  # should not exist but does
                raise HdhactionsAlreadyExistsError(f"Directory {cname} already exists.")

        return container_client

    def _safe_get_blobs(
        self,
        cname: str,
        bnames: Iterable[str],
        blob_should_exist: bool,
        partial_ok: bool = False,
    ) -> List[Optional[BlobClient]]:
        """Obtain blob clients and validate whether they exist.

        Args:
            cname: name of the parent container
            bnames: names of the blobs
            blob_should_exist: whether the blobs should already exist.
            partial_ok:
                If True, do not check for existence.
                If False, raise an error if any of the blob's existence status
                is incorrect.

        Returns: the list of blob clients

        Raises:
            HdhactionsMissingError: if a blob should exist but does not
            HdhactionsAlreadyExistsError: if it should not exist but it does
        """
        return list(
            self._safe_get_iterate_blobs(cname, bnames, blob_should_exist, partial_ok)
        )

    def _safe_get_iterate_blobs(
        self,
        cname: str,
        bnames: Iterable[str],
        blob_should_exist: bool,
        partial_ok: bool = False,
    ) -> Iterable[Optional[BlobClient]]:
        """Obtain an iterator of blob clients and validate whether they exist.

        Args:
            cname: name of the parent container
            bnames: names of the blobs
            blob_should_exist: whether the blobs should already exist.
            partial_ok:
                If True, do not check for existence.
                If False, raise an error if any of the blob's existence status
                is incorrect.

        Returns: a generator(iterator) of blob clients

        Raises:
            HdhactionsInvalidContainerName: if the container name is invalid
            HdhactionsMissingError: if a blob should exist but does not
            HdhactionsAlreadyExistsError: if it should not exist but it does
        """

        # Necessarily, we want the directory to exist

        container_client = self._safe_get_container(cname, True)

        # TODO: container_client.list_blobs() might allow better perf than the loop?
        for blobname in bnames:

            blob_client = container_client.get_blob_client(blobname)

            if (not partial_ok) and blob_client.exists() != blob_should_exist:
                if blob_should_exist:  # should exist but does not
                    raise HdhactionsMissingError(
                        f"File {blobname} does not exist in directory {cname}."
                    )
                else:  # should not exist but does
                    raise HdhactionsAlreadyExistsError(
                        f"File {blobname} already exists in directory {cname}."
                    )
            else:
                yield blob_client

    def list_directories(self) -> List[str]:
        """Get the list of directories in the Azure storage.

        Returns: a list of the names of directories in the storage.
        """
        containers = self.blob_service_client.list_containers()
        return [container.name for container in containers]

        # containers = self.fs.ls("/")
        # return containers

    def create_directory(self, directory: str, *, verbose: bool = True):
        """Create a new container in the Azure storage.

        This function can only be used for containers in the Azure sense.
        If you are using

        Args:
            directory: name of directory to create
            verbose: whether to print confirmation on directory creation

        Raises:
            HdhactionsInvalidContainerName: if the container name is invalid
            HdhactionsAlreadyExistsError: if the directory already exists
        """
        cclient = self._safe_get_container(directory, False)

        cclient.create_container()
        if verbose:
            print(f"Directory {directory} was created.")

    def delete_directory(self, directory: str, *, verbose: bool = True):
        """Delete the directory with all the files it contains.

        Args:
            directory: name of directory to delete
            verbose: whether to confirm directory suppression

        Raises:
            HdhactionsInvalidContainerName: if the container name is invalid
            HdhactionsMissingError: if the directory does not exist
        """
        cclient = self._safe_get_container(directory, True)

        cclient.delete_container()
        if verbose:
            print(f"Directory {directory} was deleted.")

    def list_files(
        self, directory: str, metadata: bool = False
    ) -> Union[List[str], List[Dict]]:
        """Get the list of the files in a given directory.

        Args:
            directory: name of directory whose file list to read
            metadata: whether to return a list of file names or the metadata

        Returns:
            a list of strings (the file names) if metadata is False,
            else a list of dicts (blobs)

        Raises:
            HdhactionsMissingError: if the directory is not present
        """

        return list(self.iterate_files(directory=directory, metadata=metadata))

    def iterate_files(
        self, directory: str, metadata: bool = False
    ) -> Union[Iterable[str], Iterable[Dict]]:
        """Get the iterator of the files in a given directory.

        Args:
            directory: name of directory whose file list to read
            metadata: whether to return a only file names or with the metadata

        Returns:
            a generator (iterator) of blobs or blobs name

        Raises:
            HdhactionsMissingError: if the directory is not present
        """

        container_client = self._safe_get_container(directory, True)
        iter_blobs = container_client.list_blobs()

        if metadata:
            return iter_blobs
        else:
            return (blob.name for blob in iter_blobs)

    def get_properties(
        self,
        directory: str,
        files: Union[str, Iterable[str]],
        missing_ok: bool = True,
    ) -> List[dict]:
        """Get the last modification date of a file or a list of files.

        Args:
            directory: Name of the directory to investigate
            files: name(s) of the file(s) to investigate

        Returns: a list of properties, one for each file(s)

        Raises:
            HdhactionsMissingError: if the directory is not present,
                or if missing_ok is False and any of the files are missing
        """

        if isinstance(files, str):
            files = [files]

        liste = []
        for file in files:
            try:
                liste.append(self.fs.info(f"{directory}/{file}"))
            except FileNotFoundError:
                if missing_ok:
                    continue
                else:
                    raise HdhactionsMissingError(f" file {file} doesn't exist")

        return liste

    def get_iterate_properties(
        self,
        directory: str,
        files: Union[str, Iterable[str]],
        missing_ok: bool = True,
    ) -> Iterable[dict]:
        """Get the properties of a file or a list of files.

        Args:
            directory: Name of the directory to investigate
            files: name(s) of the file(s) to investigate

        Returns: a generator (iterator) of properties

        Raises:
            HdhactionsMissingError: if the directory is not present,
                or if missing_ok is False and any of the files are missing
        """

        if isinstance(files, str):
            files = [files]

        blobs = self._safe_get_iterate_blobs(
            directory, files, blob_should_exist=True, partial_ok=missing_ok
        )

        return (bc.get_blob_properties() for bc in blobs)

    def push_data(
        self,
        directory: str,
        remote_filename: str,
        data: pd.DataFrame,
        overwrite: bool = False,
    ) -> None:
        """Write data in the given directory.

        This function is deprecated. Write to disk and use push_file instead.

        Args:
            directory: name of the directory in which to store the resulting file.
            remote_filename: name of the file in which the data will be stored.
            data: the data that is to be stored in the file.

        Keyword args:
            overwrite: whether to overwrite an existing file
        """

        warnings.warn(
            "Write data to the local disk then use push_file() instead.",
            DeprecationWarning,
        )

        blob_client = self._safe_get_blobs(
            directory, [remote_filename], blob_should_exist=False, partial_ok=overwrite
        )[0]

        ext = remote_filename.split(".")[-1]
        if ext == "csv":
            data = data.to_csv(index=False)
        elif ext == "json":
            data = json.dumps(data)
        elif ext == "pkl":
            data = pkl.dumps(data)
        blob_client.upload_blob(data, overwrite=overwrite)

    def push_file(
        self,
        directory: str,
        local_filename: str,
        remote_filename: str = None,
        metadata: Optional[Dict] = None,
        overwrite: bool = False,
        verbose: bool = True,
    ) -> None:
        """Upload a local file to the given Azure directory.

        Args:
            directory: name of the directory in which to store the file.
            local_filename: name of the file in the local storage

        Keyword args:
            remote_filename: name of the file in the remote storage
                If not given or None (default), same as the local name.
            metadata: the metadata dict to push with the file, or None
            overwrite: whether to overwrite the file if it already exists
            verbose: whether to print a confirmation after pushing the file
        """

        if remote_filename is None:
            remote_filename = local_filename

        blob_client = self._safe_get_blobs(
            directory, [remote_filename], blob_should_exist=False, partial_ok=overwrite
        )[0]

        with open(local_filename, "rb") as f:
            blob_client.upload_blob(f, metadata=metadata, overwrite=overwrite)

        if verbose:
            print(
                f"Local file {local_filename} well pushed as {directory}/{remote_filename}"  # noqa: E501
            )

    def push_file_from_stream(
        self,
        stream: BinaryIO,
        directory: str,
        remote_filename: str,
        metadata: Union[dict, None] = None,
        overwrite: bool = False,
        verbose: bool = True,
    ) -> None:
        """Push local file (from stream pointer) to Azure storage.

        You might want to call .seek(0) on the stream pointer elsewhere if
        needed; by default, we allow non-seekable streams, so no reset of the
        cursor is done.

        Args:
            stream: binary stream (any object with a .read method)
            directory: name of the directory in which to store the file.
            remote_filename: name of the file to push
        Keyword args:
            metadata: The metadata to push with the file, if any (default None)
            overwrite: whether to overwrite the file if it already exists
            verbose: whether to print a confirmation after pushing the file
        """

        blob_client = self._safe_get_blobs(
            directory, [remote_filename], blob_should_exist=False, partial_ok=overwrite
        )[0]

        blob_client.upload_blob(stream, metadata=metadata, overwrite=overwrite)
        if verbose:
            print(f"File well pushed as {directory}/{remote_filename}")

    def read_file_as_stream(
        self, directory: str, filename: str
    ) -> StorageStreamDownloader:
        """Load the data of a file in a given directory.

        Args:
            directory: name of the directory
            filename: name of the file to read from
        """

        blob_client = self._safe_get_blobs(
            directory, [filename], blob_should_exist=True, partial_ok=False
        )[0]

        download_stream = blob_client.download_blob()
        return download_stream

    def read_file(
        self,
        directory: str,
        filename: str,
        zip: bool = False,
        fmt: str = None,
        **kwargs,
    ) -> Any:
        """Load the data of a file in a given directory.

        Deprecated: use the format-specific functions directly if you can.

        Args:
            directory: name of the directory
            filename: name of the file in the directory
            zip: Whether or not the file is a zip file
            fmt: format of the file. If None, inferred from filename.

        Returns: in-memory data, parsed in a relevant format:
            csv -> pandas.DataFrame
            txt -> str
            json -> dict
            hdf5 -> h5py.File
            pkl -> anything it contained
        """

        warnings.warn(
            "Use format-specific functions instead of read_file() if possible.",
            DeprecationWarning,
        )

        if fmt is None:
            fmt = filename.split(".")[-1]

        if fmt == "csv":
            if zip and kwargs.get("compression", None) is None:
                raise ValueError(
                    "If zip is True, you must specify a compression algorithm in the pandas kwargs."  # noqa: E501
                )
            if (not zip) and (kwargs.get("compression", None) is not None):
                raise ValueError(
                    "If zip is False, you should not specify a compression algorithm in the pandas kwargs."  # noqa: E501
                )

            return self.read_as_df(directory, filename, **kwargs)

        elif fmt == "txt":
            return self.read_as_txt(directory, filename, zip=zip, **kwargs)

        elif fmt == "json":
            return self.read_file_json_into_dict(directory, filename, zip=zip, **kwargs)

        # TODO: split off the rest into their own functions

        data = self.read_file_as_stream(directory, filename)

        if zip:
            stream_bytes = BytesIO()
            data.download_to_stream(stream_bytes)
            stream_bytes.seek(0)
        if fmt == "hdf5":
            print("Requires to download the file. Proceeding...")
            self.download_file(directory, filename)
            return h5py.File(filename, "r")
        elif fmt == "pkl":
            return pkl.loads(data.content_as_bytes())

        return stream_bytes if zip else data

    def read_as_df(
        self,
        directory: str,
        filename: str,
        **kwargs,
    ):
        """Reads csv file in-memory. Equivalent to pandas.read_csv.

        Args:
            directory: name of the directory
            filename: name of the file in the directory

        Keyword args:
            **kwargs: passed to pandas.read_csv()

        To pass a zip file, specify compression="zip" or similar in the kwargs.
        """

        raw_data = self.read_file_as_stream(directory, filename)
        stream_bytes = BytesIO()
        raw_data.readinto(stream_bytes)
        stream_bytes.seek(0)
        return pd.read_csv(stream_bytes, **kwargs)

    def read_as_txt(
        self,
        directory: str,
        filename: str,
        encoding: str = "utf-8",
        zip: bool = False,
    ):
        """Reads pure txt file in memory. Equivalent to:
            with open(...) as fifi:
                return fifi.read()

        Args:
            directory: name of the directory
            filename: name of the file in the directory

        Keyword args:
            encoding: file encoding
            zip: True if the file is zipped
        """
        raw_data = self.read_file_as_stream(directory, filename)

        if zip:
            stream_bytes = BytesIO()
            raw_data.readinto(stream_bytes)
            stream_bytes.seek(0)
            with zipfile.ZipFile(stream_bytes) as zip_object:
                zip_members = zip_object.namelist()
                if len(zip_members) != 1:
                    raise ValueError(
                        f"That zip file contains {len(zip_members)} files. It should only have one."  # noqa: E501
                    )
                with zip_object.open(zip_members[0]) as unzipped_file:
                    bytes_data = unzipped_file.read()
                    return bytes_data.decode(encoding=encoding)

        else:
            return raw_data.content_as_text(encoding=encoding)

    def read_file_json_into_dict(
        self,
        directory: str,
        filename: str,
        encoding: str = "utf-8",
        zip: bool = False,
        **kwargs,
    ):
        """Reads JSON file in memory. Equivalent to:
            with open(...) as fifi:
                return json.load(fifi, **kwargs)

        Args:
            directory: name of the directory
            filename: name of the file in the directory

        Keyword args:
            encoding: file encoding
            zip: True if the file is zipped
        """

        return json.loads(
            self.read_as_txt(directory, filename, encoding=encoding, zip=zip),
            **kwargs,
        )

    def download_file(
        self,
        directory: str,
        filename: str,
        local_dir: str = None,
    ) -> None:
        """Write a remote file in the local storage.

        Args:
            directory: name of the directory
            filename: name of the file to download
            local_dir: name of the directory where the file will be downloaded.

        TODO: replace local_dir by local_path, allowing to specify a new filename
        """
        download_stream = self.read_file_as_stream(directory, filename)

        local_path = os.path.join(local_dir, filename)
        top_dir = os.path.dirname(local_path)
        # if filename contain slashes, local_dir != top_dir
        # create top dir if it does not exist yet
        os.makedirs(top_dir, exist_ok=True)

        with open(local_path, "wb") as dest_file:
            download_stream.readinto(dest_file)

    def download_file_into_stream(
        self,
        directory: str,
        filename: str,
        stream: BinaryIO,
    ) -> None:
        """Stream remote file into a memory stream.

        Args:
            directory (str): name of Azure directory (container)
            filename (str): name of the file on Azure
            stream (BinaryIO): binary stream into which to download
        """
        download_stream = self.read_file_as_stream(directory, filename)
        download_stream.readinto(stream)

    def delete_file(
        self,
        directory: str,
        filename: str,
    ) -> None:
        """Delete a given file from a given directory.

        Deprecated. Use delete_files() instead in new code.

        Args:
            directory: name of the directory
            filename: name of the file in the directory to delete
        """

        warnings.warn(
            "Use delete_files() instead of delete_file() if possible.",
            DeprecationWarning,
        )

        self.delete_files(directory, filename, strict=True)

    def delete_files(
        self,
        directory: str,
        filelist: Union[str, Iterable[str]],
        strict: bool = False,
        Nshow: int = 10,
        soft_delete: bool = False,
    ) -> None:
        """Delete files from a given directory.

        Args:
            directory: name of the directory (Azure container)
            filelist: file(s) to delete
            strict: how to proceed if some files are not present:
                if True, abort the entire operation;
                if False, delete those that exist
            Nshow: maximum number of files to show in print / errors
        Raises:
            FileNotFoundError: if strict and some of the files do not exist
        """

        # Some notes about why soft delete is not implemented

        # If we want to code a soft/hard delete switch, it should have 3 states
        # - omitted / None: follow the server default
        # - "hard": do a hard delete (always possible)
        # - "soft": do a soft delete if possible, and if not raise an error
        # In particular if the user specifically asks for a soft delete we
        # should not cause a hard delete even with a post-hoc warning.
        # However, I did not find how to do this. Investigations follow.
        #
        # There is no real doc about blob_client.delete_blob().
        # I *believe*, based on
        # https://github.com/Azure/azure-storage-python/blob/4306898850dd21617644fc537a57d025e833db74/tests/blob/test_common_blob.py#L687-L688
        # that if the function is called without a parameter, it does what we
        # want (= follow the default retention policy).
        #
        #
        # I further believe, based on rummaging through the undocumented source
        # of that method and its called methods, that:
        # - blob_client.delete_blob() takes an argument blob_delete_type
        # - that argument can be either
        #   - omitted -> follow server retention policy
        #   - None (equivalent to omitted?)
        #   - "Permanent" -> force hard delete
        # Therefore, if that is correct, there is no "soft" option that would
        # cause a graceful failure if not supported by server settings.

        # Accept single-element argument
        if isinstance(filelist, str):
            filelist = [filelist]

        # Fetch container client, and error out appropriately if not found
        container_client = self._safe_get_container(directory, True)

        # Compare existing files vs. delete instructions
        blobs_list = [blob.name for blob in container_client.list_blobs()]
        missing = set(filelist) - set(blobs_list)

        if missing:

            miss_ex = _head_fmt(sorted(missing), Nshow)

            if strict:
                raise FileNotFoundError(
                    f"Erreur: {len(missing)} fichier(s) manquant(s) dans {directory} : {miss_ex}"  # noqa: E501
                )
            else:
                print(
                    f"Note: {len(missing)} fichier(s) manquant(s) dans {directory}: {miss_ex}.\nLes fichiers manquants seront ignorés."  # noqa: E501
                )

        # Do the deletion of actually-present files
        delete_list = sorted(set(filelist) & set(blobs_list))

        # If there are none, exit early
        if not delete_list:
            print("Aucun fichier à effacer !")
            return

        # Note: we can only delete by batches of at most 256:
        # https://learn.microsoft.com/en-us/python/api/azure-storage-blob/azure.storage.blob.containerclient?view=azure-python#azure-storage-blob-containerclient-delete-blobs
        # so we group the request by batches of that size
        for chunk_to_delete in _iterable_chunker(delete_list, 256):
            container_client.delete_blobs(*chunk_to_delete)

        print(f"Deleted {len(delete_list)} file(s): {_head_fmt(delete_list, Nshow)}")

    def list_deleted_files(self, directory: str) -> List[Dict[str, List[str]]]:
        """Get the list of deleted files from a given directory.

        Args:
            directory: name of the directory (Azure container)
        """

        # Fetch container client, and error out appropriately if not found
        container_client = self._safe_get_container(directory, True)

        # list  the blobs in the container including the deleted ones
        list_all_blobs = list(
            container_client.list_blobs(include=["deleted", "versions"])
        )

        # list the blobs in the container without the deleted ones
        list_active_blobs = list(container_client.list_blobs())

        # Initiate the dicos
        all_blobs = defaultdict(list)
        active_blobs = defaultdict(list)

        # construct the dico
        for blob in list_all_blobs:
            if blob in list_active_blobs:
                active_blobs[blob.name] += [blob.version_id]
            all_blobs[blob.name] += [blob.version_id]

        # remove the active blobs from all blobs
        deleted_blobs = {
            k: set(all_blobs[k]) - set(active_blobs[k]) for k in all_blobs.keys()
        }

        # remove the blobs that have no deleted version
        filtered_deleted = {k: v for k, v in deleted_blobs.items() if v != set()}

        return filtered_deleted

    def restore_version_file(self, directory: str, file: str, version: str):
        """Restore a version of a file from a given directory.

        Function to use only when versionning is enabled

        start_copy_from_url: create a new file that is a copy of the file deleted,
        this is why when executing the list_deleted_files() they delted file is still there,
        but when executing list_bobs(), we see the file is restored

        Args:
            directory: name of the directory (Azure container)
            file: name of the file to restore
            version: version_id of the file
        """

        container_client = self._safe_get_container(directory, True)
        blob = container_client.get_blob_client(file)

        # url
        versioned_blob_url = f"{blob.url}?versionId={version}"

        # restore blob
        blob.start_copy_from_url(versioned_blob_url)

        logging.info(
            f"blob {blob.blob_name} version {version} was succesfully restored"
        )

    def restore_latest_version_files(self, directory: str, filelist: str):
        """Restore last version of some files from a given directory.

        This function only restore the files from the list that are deleted,
        it skips the rest

        Args:
            directory: name of the directory (Azure container)
            file: name of the file to restore
            version: version_id of the file to be restored

        raises:
            HdhactionsMissingError:
                When all the input files are not in the list_deleted_files,
                either blob name incorrect or blob still active
        """
        container_client = self._safe_get_container(directory, True)

        # get the blobs the user want to restore
        blobs = []
        for file in filelist:
            blob_client = container_client.get_blob_client(file)
            blobs.append(blob_client)

        # get the deleted blobs
        list_deleted_blobs = self.list_deleted_files(directory)

        # get the deleted versions of the blobs the user want
        blob_versions = {}
        saved_blobs = []
        for b in blobs:
            if b.blob_name in list_deleted_blobs.keys():
                blob_versions[b.url] = list(list_deleted_blobs[b.blob_name])
                saved_blobs.append(b)

        # in case the files dont exist
        if blob_versions == {}:
            raise HdhactionsMissingError("File not found within the deleted ones")
        else:
            # get the latest version for each deleted file
            sortList = {k: sorted(v) for k, v in blob_versions.items()}
            latest_versions = {k: v[-1] for k, v in sortList.items()}

            # restore the blobs
            for (i, (url, version)) in enumerate(latest_versions.items()):
                versioned_blob_url = f"{url}?versionId={version}"
                saved_blobs[i].start_copy_from_url(versioned_blob_url)

            logging.info("blobs were succesfully restored")

    def restore_softdeleted_file(self, directory: str, file: str):
        """Restore a softdeleted file .

        This function is to be used only when softdeleted is enabled and not versioning

        Args:
            directory: name of the directory (Azure container)
            file: name of the file to restore

        raises:
            HdhactionsMissingError:
                When the file is not deleted (still active)
            ResourceNotFoundError:
                If the blob's name doesn't exist (nor active, nor deleted)
        """

        container_client = self._safe_get_container(directory, True)
        blob = container_client.get_blob_client(file)

        # restore blob
        blob.undelete_blob()

        all_names = [
            (blob.name, blob.version_id) for blob in container_client.list_blobs()
        ]
        if file in all_names:
            logging.info(f"the blob {file} was succesfully restored")
        else:
            raise HdhactionsMissingError("the file couldn't be restored")

    def restore_file(self, directory: str, file: str):
        """Restore a file

        Args:
            directory: name of the directory (Azure container)
            file: name of the file to restore

        raises:
            HdhactionsMissingError:
                When we can't restore the file (it means it was hard deleted)

        """

        try:
            self.restore_softdeleted_file(directory, file)
        except HdhactionsMissingError:
            try:
                self.restore_latest_version_files(directory, [file])
            except HdhactionsMissingError:
                raise HdhactionsMissingError(
                    "!ERROR! The file is either lost forever or not deleted "
                )

    def get_tags(self, directory: str, file: str) -> Dict:
        """Return all tags for a given file."""
        bc = self._safe_get_blobs(directory, [file], blob_should_exist=True)[0]
        return bc.get_blob_tags()

    def set_tags(
        self,
        directory: str,
        file: str,
        keep_nonmodified_tags: bool = True,
        /,
        **new_tags_and_values: Dict,
    ) -> None:
        """Set all tags, or update given tags, for a file."""
        bc = self._safe_get_blobs(directory, [file], blob_should_exist=True)[0]

        if keep_nonmodified_tags:
            current_tags = bc.get_blob_tags()
            new_tags = current_tags.copy()
            new_tags.update(new_tags_and_values)
        else:
            new_tags = new_tags_and_values

        bc.set_blob_tags(new_tags)
