#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from typing import Callable, List, Union

import adlfs

from .azurestorage import AzureStorage, HdhactionsError, HdhactionsMissingError

logger = logging.getLogger(__name__)


def print_storage_contents(
    azs: AzureStorage,
    Nfiles: int = 10,
) -> None:
    """
    Display all contents of a single storage in a human-readable way.

    Args:
        azs (AzureStorage): the Azure storage to investigate.
    Keyword args:
        Nfiles (int): maximum number of files to display (per container).
    """

    containers = azs.list_directories()
    print(f"++{len(containers)} container(s): {containers}++")
    for cont in containers:
        flist = azs.list_files(directory=cont)
        print(f"==={cont}: {len(flist)} file(s)===")
        for i, f in enumerate(flist):
            if i >= Nfiles:
                print(f"...and {len(flist)-Nfiles} other file(s).")
                break
            print(f)


def dezip(
    azs_in: AzureStorage,
    cont_in: str,
    files_in: Union[List[str], str],
    azs_out: AzureStorage,
    cont_out: str,
    files_out: Union[List[str], str],
    *,
    skip_missing: bool = True,
    overwrite_existing: bool = False,
):

    """


    Keyword args:
        skip_missing (bool): how to react if an expected input file does not exist.
            If True (default), skip and proceed.
            If False, error immediately. In that case, files that have already been processed
            up to this point will not be deleted.
        overwrite_existing (bool): how to react if an output file already exists.
            Passed unmodified to azure.storage.blob.BlobClient.upload_blob
    """
    # Argument formatting and checking
    if isinstance(files_in, str):
        files_in = [files_in]

    if isinstance(files_out, str):
        files_out = [files_out]

    if len(files_out) != len(files_in):
        raise ValueError("Length discrepancy in input file lists.")

    # check input and output containers exist
    cc_in = azs_in._safe_get_container(cont_in, container_should_exist=True)
    cc_out = azs_out._safe_get_container(cont_out, container_should_exist=True)

    logger.info(
        f"Dezip start from {azs_in.account_name}:{cont_in} into {azs_out.account_name}:{cont_out}."
    )

    # Iterate over (source, target) blobs, dezipping source into target
    for i, (file_in, file_out) in enumerate(zip(files_in, files_out)):
        logger.debug(f"Dezip {i+1}/{len(files_in)}: {file_in} -> {file_out}")

        # Check existence of input blob
        bc_in = cc_in.get_blob_client(file_in)
        if not bc_in.exists():
            if skip_missing:
                logger.warning(f"Dezip: skipping non-existing input file {file_in}")
                continue
            else:
                raise HdhactionsMissingError(
                    f"Missing input blob: {file_in} in {azs_in.account_name}:{cont_in}"
                )

        # Non-existence of output blob is not checked; this will be handled by
        # azure.storage.blob.BlobClient.upload_blob(..., overwrite=...)
        bc_out = cc_out.get_blob_client(file_out)

        # Do the dezip-upload via adlfs
        fs_input = adlfs.AzureBlobFileSystem(
            account_name=azs_in.account_name, credential=azs_in.get_credential()
        )

        with fs_input.open(f"{cont_in}/{file_in}", compression="zip") as f:
            bc_out.upload_blob(f, overwrite=overwrite_existing)

        logger.debug(f"Successful dezip: {file_in} -> {file_out}")


def copy_between_storages(
    source_storage: AzureStorage,
    source_directory: str,
    target_storage: AzureStorage,
    target_directory: str,
    source_files: list,
    copy_metadata: bool = False,
    rename: Union[None, Callable, List] = None,
    missing_ok: bool = False,
    overwrite_ok: bool = False,
    unzip: bool = False,
    local_download: bool = True,
) -> None:
    """Copy multiple files across storages.

    Args:
        source_storage: the Azure storage from which files will be copied.
        source_directory: the container in the source storage.
        target_storage: the Azure storage to which files will be copied.
        target_directory: the container in the target storage.
        source_files: List of the files to copy.

    Keyword args:
        copy_metadata: Whether to copy blob metadata.
        rename: Indicates how to rename the files.
            If None, the original names (from source_files) are kept.
            If it is a list, it must have the same length as source_files.
            If it is a function, it is applied on each source file name.
            In any case, the new names must be valid blob names.
        missing_ok: how to handle missing source files.
            If True, skip those.
            If False, exit without doing anything (ignore existing sources too).
        overwrite_ok: how to handle target files that already exist.
            If True, they will be overwritten.
            If False, exit without doing anything (ignore non-conflicting
            targets too).
        unzip: whether to unzip the file contents.
            Not implemented yet: a True value causes an error.
        local_download: whether to download locally or to stream.
            Not implemented yet: a False value causes an error.
    """

    # TODO: allow to read/write as a stream
    if not local_download:
        raise NotImplementedError("Stream-only treatment is not available yet.")

    # TODO: allow to unzip on the fly
    if unzip:
        raise NotImplementedError("Unzipping is not available yet.")

    # First, check that the source and target directories exist
    # If not, there is no fixing that, so error out
    sdirs = source_storage.list_directories()
    if source_directory not in sdirs:
        raise HdhactionsError(
            f"The source storage does not contain a container named {source_directory}. Available containers are: {sdirs}."  # noqa: E501
        )

    tdirs = target_storage.list_directories()
    if target_directory not in tdirs:
        raise HdhactionsError(
            f"The target storage does not contain a container named {target_directory}. Available containers are: {tdirs}."  # noqa: E501
        )

    # Then, compute the list of (source, target) locations
    # Also, if the user wants to copy metadata, they probably need to keep the
    # same names, so error out in that case
    if rename is None:
        # If we gave no instruction, move to the same names
        copylist = [(f, f) for f in source_files]
    else:
        # We gave an instruction to move the file
        # First, error out if metadata was supposed to be kept
        if copy_metadata:
            raise HdhactionsError("You cannot both rename files and keep metadata.")
        # Then, if the instruction was a list of new files, just use those
        try:
            if len(rename) != len(source_files):
                raise ValueError(
                    f"""Source and target lists have different sizes:
                        Source:{source_files}
                        Target:{rename}"""
                )
            copylist = list(zip(source_files, rename))
        except TypeError:
            # We gave a function, which is not iterable, just apply it
            copylist = [(f, rename(f)) for f in source_files]

    # Check if the source files exist and the target files do not
    scontents = source_storage.list_files(directory=source_directory)
    missing_files = set(f for f, _ in copylist) - set(scontents)
    if missing_files:
        msg = f"{len(missing_files)} files are missing from the source storage:\n{sorted(missing_files)}"  # noqa: E501
        if missing_ok:
            logging.info(msg)
        else:
            raise ValueError(msg)

    tcontents = target_storage.list_files(directory=target_directory)
    existing_files = set(f for _, f in copylist) & set(tcontents)
    if existing_files:
        msg = f"{len(existing_files)} files already exist in the target storage:\n{sorted(existing_files)}"  # noqa: E501
        if overwrite_ok:
            logging.info(msg)
        else:
            raise ValueError(msg)

    # Actually run the operations
    for i, (source_fn, target_fn) in enumerate(copylist):
        logging.warning(f"{i+1}/{len(copylist)}: {source_fn} -> {target_fn}")
        # If the source file does not exist, skip that file
        if source_fn not in scontents:
            logging.warning("Skipped: source file does not exist")
            continue
        # If the target file exists, overwrite it
        if target_fn in tcontents:
            logging.warning("Note: file already exists, but will be overwritten")

        # Right now, only local-download-and-upload is implemented
        # For each blob, we create a temporary local file
        path_source = source_directory + "/" + source_fn
        path_dest = target_directory + "/" + target_fn

        with source_storage.fs.open(path_source, "rb") as source_file:
            with target_storage.fs.open(path_dest, "wb") as destination_file:
                destination_file.write(source_file.read())

        # If appropriate, we look up the metadata
        if copy_metadata:
            metadata = source_storage.get_properties(source_directory, source_fn)[0][
                "metadata"
            ]
        else:
            metadata = None

        logging.warning(f"metadata: {metadata}")
        destination_blob_client = target_storage.blob_service_client.get_blob_client(
            container=target_directory, blob=target_fn
        )
        try:
            destination_blob_client.set_blob_metadata(metadata=metadata)
        except Exception:
            raise HdhactionsError(f"The metdata can't be added to the file {target_fn}")

        logging.warning(
            f"metadata dans storage: {target_storage.fs.ls(path_dest, detail=True)}"
        )

    # Confirmation message
    print("Mass-copying finished!")
