# -*- coding: utf-8 -*-
import random
import string

import pytest

from hdhactions.storage.azurestorage import (
    HdhactionsAlreadyExistsError,
    HdhactionsInvalidContainerName,
    HdhactionsMissingError,
)


@pytest.fixture()
def setup_crud(storageTest):
    # storageTest is supposed to be wiped between uses
    yield storageTest


def _random_string(L=10):
    return "".join(random.choices(string.ascii_lowercase, k=L))


@pytest.mark.parametrize("cont_name", [_random_string() for _ in range(3)])
def test_dir_create(setup_crud, cont_name):
    # First creation goes fine
    setup_crud.create_directory(cont_name)
    # Attempt to create the same name fails
    with pytest.raises(HdhactionsAlreadyExistsError):
        setup_crud.create_directory(cont_name)


@pytest.mark.parametrize(
    "invalid_cont_name",
    [
        "aa",
        "consecutive--hyphens",
        "CAPS",
    ],
)
def test_dir_create_invalid_name(setup_crud, invalid_cont_name):
    with pytest.raises(HdhactionsInvalidContainerName):
        setup_crud.create_directory(invalid_cont_name)


@pytest.mark.parametrize("cont_name", [_random_string() for _ in range(3)])
def test_dir_delete(setup_crud, cont_name):
    # Deleting a non-existing directory fails
    with pytest.raises(HdhactionsMissingError):
        setup_crud.delete_directory(cont_name)

    # Creating, then deleting, goes fine
    setup_crud.create_directory(cont_name)
    setup_crud.delete_directory(cont_name)

    # Trying to re-delete again fails
    with pytest.raises(HdhactionsMissingError):
        setup_crud.delete_directory(cont_name)
