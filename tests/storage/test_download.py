#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import random
import string

import pytest


@pytest.fixture()
def setup(storageTest):
    # Setup directories for unit testing
    yield storageTest.create_directory(directory="unittest")


def _random_string(L=10):
    return "".join(random.choices(string.ascii_lowercase, k=L))


@pytest.mark.parametrize("txtcontent", [_random_string() for _ in range(5)])
def test_upload_download(setup, storageTest, txtcontent):
    """Upload and download a small test file."""

    try:
        filename = "tmp.txt"
        local_file = os.path.abspath(filename)
        directory = "unittest"

        bytecontent = txtcontent.encode(encoding="utf8")

        # write locally
        with open(local_file, "wb") as fifi:
            fifi.write(bytecontent)

        # Push the file with that encoding
        storageTest.push_file(
            directory,
            local_filename=local_file,
            remote_filename=filename,
            overwrite=True,
        )

        # Remove local file
        os.remove(local_file)

        # Download from storage again
        storageTest.download_file(
            directory,
            filename=filename,
            local_dir=os.path.dirname(local_file),
        )

        # Compare the results
        with open(local_file, "rb") as result_file:
            result_bytes = result_file.read()
            assert result_bytes == bytecontent

    finally:
        if os.path.exists(local_file):
            os.remove(local_file)
        # storageTest.delete_files(directory, [filename], strict=False)
